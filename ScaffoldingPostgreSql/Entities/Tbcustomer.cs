﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScaffoldingPostgreSql.Entities
{
    [Table("tbcustomer")]
    public partial class Tbcustomer
    {
        [Column("customerid")]
        public int Customerid { get; set; }
        [Required]
        [Column("customername")]
        [StringLength(50)]
        public string Customername { get; set; }
    }
}
