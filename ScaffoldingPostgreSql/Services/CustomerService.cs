﻿using Microsoft.EntityFrameworkCore;
using ScaffoldingPostgreSql.Entities;
using ScaffoldingPostgreSql.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScaffoldingPostgreSql.Services
{
    public class CustomerService
    {
        private readonly AppDbContext _AppDbContext;

        public List<CustomerViewModel> ListCustomer { set; get; } = new List<CustomerViewModel>();

        public CustomerService(AppDbContext appDbContext)
        {
            this._AppDbContext = appDbContext;
        }

        public async Task<List<CustomerViewModel>> GetAllCustomerLinq()
        {
            var getAllData = await this._AppDbContext.Tbcustomer
                .AsNoTracking()
                .Select(Q => new CustomerViewModel
                {
                    CustomerId = Q.Customerid,
                    CustomerName = Q.Customername
                }).ToListAsync();

            return getAllData;
        }
    }
}
