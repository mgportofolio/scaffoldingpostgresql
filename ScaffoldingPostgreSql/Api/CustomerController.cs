﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ScaffoldingPostgreSql.Services;
using ScaffoldingPostgreSql.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ScaffoldingPostgreSql.Api
{
    [Route("api/v1/customer")]///[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CustomerController : Controller
    {
        private readonly CustomerService _CustomerService;
        private readonly IConfiguration _Config;

        public CustomerViewModel FormCustomer { set; get; }

        public CustomerController(CustomerService customerService, IConfiguration configuration)
        {
            this._CustomerService = customerService;
            this._Config = configuration;
        }
        // GET: api/<controller>
        [HttpGet("ApiKeyGetAllCustomer", Name = "get-all-customer")]
        public async Task<ActionResult<List<CustomerViewModel>>> GetAllCustomerApi()
        {
            var apiKey = Request.Headers["ApiKey"];
            if (string.IsNullOrEmpty(apiKey) || apiKey != _Config["ApiKey"])
            {
                return BadRequest("Invalid Api Key");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }
            var allData = await _CustomerService.GetAllCustomerLinq();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
