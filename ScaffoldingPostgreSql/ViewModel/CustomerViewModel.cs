﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScaffoldingPostgreSql.ViewModel
{
    public class CustomerViewModel
    {
        public int CustomerId { set; get; }
        public string CustomerName { set; get; }
    }
}
