using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using ScaffoldingPostgreSql.ViewModel;

namespace ScaffoldingPostgreSql.Pages.Customer
{
    public class IndexModel : PageModel
    {
        private readonly IConfiguration _Config;
        private readonly IHttpClientFactory _HttpFac;

        public IndexModel(IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            this._Config = configuration;
            this._HttpFac = httpClientFactory;
        }

        public List<CustomerViewModel> Customers { set; get; }
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:55846/api/v1/customer/ApiKeyGetAllCustomer";
            var client = _HttpFac.CreateClient();
            var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

            request.Headers.Add("ApiKey", _Config["ApiKey"]);
            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                Customers = await response.Content.ReadAsAsync<List<CustomerViewModel>>();
            }
            else
            {
                return BadRequest();
            }

            return Page();
        }
    }
}